package live.limbs.cardsmobile.rest;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class BaseConfig {
    @Bean
    public Queue hello() {
        return new Queue("main-queue");
    }
}
