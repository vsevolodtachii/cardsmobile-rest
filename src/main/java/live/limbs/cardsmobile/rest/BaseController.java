package live.limbs.cardsmobile.rest;


import live.limbs.cardsmobile.rest.data.AbstractBaseRequest;
import live.limbs.cardsmobile.rest.data.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller("/request")
public class BaseController {

    private final RequestProcessService requestProcessService;

    @Autowired
    public BaseController(RequestProcessService requestProcessService) {
        this.requestProcessService = requestProcessService;
    }


    @PostMapping
    @ResponseBody
    public BaseResponse add(@RequestBody AbstractBaseRequest request){
        requestProcessService.process(request);
        return new BaseResponse( request.getId());
    }

    @GetMapping("/{id}")
    @ResponseBody
    public AbstractBaseRequest get(@PathVariable String id){
        Optional<AbstractBaseRequest> r = requestProcessService.getById(id);
        if(r.isPresent()){
            return r.get();
        }
        else {
            throw new  RequestNotFoundException();
        }
    }
}
