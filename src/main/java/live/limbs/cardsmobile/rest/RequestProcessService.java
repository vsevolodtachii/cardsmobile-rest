package live.limbs.cardsmobile.rest;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import live.limbs.cardsmobile.rest.data.AbstractBaseRequest;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RequestProcessService {

    private final ObjectMapper jacksonObjectMapper;

    private final RabbitTemplate template;

    private final Queue queue;

    private final BaseRequestRepository repository;

    @Autowired
    public RequestProcessService(ObjectMapper jacksonObjectMapper, RabbitTemplate template, Queue queue, BaseRequestRepository repository) {
        this.jacksonObjectMapper = jacksonObjectMapper;
        this.template = template;
        this.queue = queue;
        this.repository = repository;
    }

    public void process(AbstractBaseRequest request) {

        try {
            template.convertAndSend(queue.getName(), jacksonObjectMapper.writeValueAsString(request));
        } catch (JsonProcessingException e) {
            //TO DO logger
        }
    }

    public Optional<AbstractBaseRequest> getById(String id){
        return repository.findById(id);
    }
}
