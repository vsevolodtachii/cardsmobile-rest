package live.limbs.cardsmobile.rest.data;


import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

@Entity
@Table(name = "IntRequest")
public class IntRequest extends AbstractBaseRequest {

    @OrderColumn
    @ElementCollection
    private Integer[] data;

    public Integer[] getData() {
        return data;
    }

    public void setData(Integer[] data) {
        this.data = data;
    }
}
