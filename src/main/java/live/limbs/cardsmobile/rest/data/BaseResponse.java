package live.limbs.cardsmobile.rest.data;

public class BaseResponse {
    private String id;
    private String message;

    private BaseResponse() {
    }

    public BaseResponse(String id) {
        this.id = id;
        this.message = "OK";
    }


    public String getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }
}
