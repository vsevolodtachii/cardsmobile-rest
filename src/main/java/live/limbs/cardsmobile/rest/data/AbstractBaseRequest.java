package live.limbs.cardsmobile.rest.data;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS,
        property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type( name = "TextRequest", value = TextRequest.class),
        @JsonSubTypes.Type( name = "IntRequest", value = IntRequest.class)
})
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name="descriminatorColumn")
@Table(name="AbstractBaseRequest")
public class AbstractBaseRequest implements Serializable {

    @Id
    private String id;


    AbstractBaseRequest() {
        this.id = UUID.randomUUID().toString();
    }


    public String getId(){
        return this.id;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof AbstractBaseRequest)) {
            return false;
        }
        AbstractBaseRequest other = (AbstractBaseRequest) obj;
        return getId().equals(other.getId());
    }

}
