package live.limbs.cardsmobile.rest.data;


import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TextRequest")
public class TextRequest extends AbstractBaseRequest {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
