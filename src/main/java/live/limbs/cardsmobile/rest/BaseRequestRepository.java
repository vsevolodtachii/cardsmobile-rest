package live.limbs.cardsmobile.rest;

import live.limbs.cardsmobile.rest.data.AbstractBaseRequest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BaseRequestRepository extends JpaRepository<AbstractBaseRequest,String> {
}
