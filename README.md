# Cardsmobile test project

### The project consists of two parts (this is first)
The second part is here:
* [->second part](https://gitlab.com/vsevolodtachii/cardsmobile-amqp-back)

### Pre requests
* docker installed 
* docker-compose installed
* JRE version > 1.8


### Guides
To run MySql and RabbitMQ:
```bash
docker-compose -f .\rabbit_mysql_compose.yml up
```

To build project:
```bash
git pull git@gitlab.com:vsevolodtachii/cardsmobile-rest.git
cd cardsmobile-rest
docker run --rm -v "$PWD":/home/gradle/project -w /home/gradle/project gradle gradle build
```

To run project:
```bash
java -jar  build/libs/rest-0.0.1-SNAPSHOT.jar
```

Next you must build and run second part:
```bash
git pull git@gitlab.com:vsevolodtachii/cardsmobile-amqp-back.git
cd cardsmobile-amqp-back
docker run --rm -v "$PWD":/home/gradle/project -w /home/gradle/project gradle gradle build
java -jar  build/libs/amqpback-0.0.1-SNAPSHOT.jar
```

##First time you start environment you need to start rest-project (this) first but amqp-project second    

After that, you can test project:
* add data type 1
```bash
 curl -X POST \
   http://127.0.0.1:5599/add \
   -H 'Content-Type: application/json' \
   -H 'cache-control: no-cache' \
   -d '{
 	"@class": ".TextRequest",
 	"text": "test_super text x2"
 }'
 ```
 * add data type 2
 ```bash
 curl -X POST \
   http://127.0.0.1:5599/add \
   -H 'Content-Type: application/json' \
   -H 'cache-control: no-cache' \
   -d '{
 	"@class": ".IntRequest",
 	"data": [4,4,5,12,16]
 }'
 ```
 * return format
 ```json
{
    "id": "9062a738-3a84-4ae8-bffb-70711250d0ba",
    "message": "OK"
}

```
* get data
```bash
curl -X GET \
  http://127.0.0.1:5599/{{ID_FROM_PREVIOS_RESPONCE}} \
  -H 'cache-control: no-cache'
```